# EPAM_101_1_Materials

Developed with Unreal Engine 5

Name of the map should contain the number and the name of the lesson e.g., 101.1-Materials_Introduction 

Map should contain 4 Actors with the completed materials 
Note: you can use any Actor that you deem suitable for you (Cube, Cylinder, Sphere, SM_MatPreviewMesh_02, etc.) 

Follow along the instructions from the video and create the following Material assets: 

MasterMat: 

1. A basic Master Material that utilizes the following material inputs: Base Color, Metallic, Roughness, Normal, Ambient Occlusion 

1.1 All values for them should be parametrized 

Texture assets used in the Material: 

shmodava_4K_Albedo, shmodava_4K_Roughness, shmodava_4K_Normal, shmodava_4K_AO 

Material Instance of MasterMat: 

2. Please tweak the parameters to a way that the completed result does not resemble the original Material.

Cloth Material: 

3. Cloth Material utilizes the following material inputs: Base Color, Opacity Mask, Fuzz Color, Cloth, Metallic, Specular, Roughness, Normal 

3.1 Material Domain is set to Surface 

3.2 Domain is set to Masked 

3.3 Shading Model is set to Cloth 

3.4 Two Sided option is enabled 

Texture assets used in the Material: 

rippedCLTH_2K_Albedo, ripCLTH_2K_Cavity, RipCLTH_2K_Gloss, ripCLTH_2K_Roughness, ripCLTH_2K_Normal

Material with Panner animation 

4. A Master material with the Panner animation that utilizes the following material inputs: Base Color, Specular, Roughness, Normal, Ambient Occlusion 

4.1 Material Domain is set to Surface 

4.2 Domain is set to Opaque 

4.3 Shading Model is set to Default Lit 

Texture assets used in the Material: 

rmnkw0p_2K_Albedo, R_Metal02, rmnkw0p_2K_Gloss, rmnkw0p_2K_Roughness, rmnkw0p_2K_Normal, R_dropsN, R_dropsAO, rmnkw0p_2K_AO, rmnkw0p_2K_Cavity

![Level.](/readme/lvl.PNG "Level.")

![Master_M.](/readme/master.PNG "Master Material.")

![Instance_M.](/readme/instance.PNG "Instance Material.")

![Cloth_M.](/readme/cloth.PNG "Cloth Material.")

![Animated_M.](/readme/animated.PNG "Animated Material.")